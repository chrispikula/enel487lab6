/**
Programmer: Chris Pikula
Project: Ball Control
Date: 2016-11-15

Description: This is a program that turns on and off
LEDs via communicating to and from the console.
It can also run some timing tests, as well as use an interrupt timer
	(Via timers 2 and 3, respectively)
	
There's also an interrupt that'll run the ball control.
*/


#include "utilities.h"
//Used for turnOnABit, turnOffABit, and delay
#include "userInterface.h"

#include "timer_impl.h"
#include "time_ops.h"


//needs to be in extern mode in order to overwrite the 'weak' TIM3_IRQHandler
//see
//http://electronics.stackexchange.com/questions/7660/bug-in-keil-arm-compiler-with-interrupt-handlers-and-c

//gives us our ADC conversion result.
extern int ADC_CONVERSION_RESULT;

//Lets us know if our ADC conversion result is finished.
static bool GLOBAL_ADC_CONVERSION_FINISHED = 0;

//The current height of the ball, in terms of A2D conversion results.
static int32_t GLOBAL_BALLHEIGHT = 0;

//The current amount we are changing the ballheight by
static int16_t GLOBAL_BALLHEIGHT_DELTA = 0;

//The current value we are setting the PWM to.
static int32_t GLOBALBallPWM = 1125;

extern "C" 
{
	//Timer 3 can blink an LED
	void TIM3_IRQHandler(void)
	{
		TIM3->SR &= ~TIM_SR_UIF;
		GPIOB->ODR ^= GPIO_ODR_ODR15;	
	}
	
	//Timer2 can control the ball height
	void TIM2_IRQHandler(void)
	{
	uint16_t BallPWMUpperLimit = 750;
	int16_t BallPWMDeltaUL = 15;
	uint16_t BallPWMLowerLimit = 550;
	int16_t BallPWMDeltaLL = -15;	

		TIM2->SR &= ~TIM_SR_UIF;
		ADC1->CR2 |= ADC_CR2_SWSTART;
		//GPIOB->ODR ^= GPIO_ODR_ODR15;	
		
				if (GLOBAL_BALLHEIGHT != 0 && GLOBAL_ADC_CONVERSION_FINISHED == 1)
					GLOBAL_ADC_CONVERSION_FINISHED = 0;
			{
				
				//Set it only once
				
				//is the ballheight result below the adc result?
				//aka, are we going up?
				if (GLOBAL_BALLHEIGHT < ADC_CONVERSION_RESULT)
				{
					GLOBAL_BALLHEIGHT_DELTA += 5;
				}
				else
					//we are going down
				{
					GLOBAL_BALLHEIGHT_DELTA -= 5;
				}
				
				if(GLOBAL_BALLHEIGHT_DELTA < BallPWMDeltaLL)
				{
					GLOBAL_BALLHEIGHT_DELTA = BallPWMDeltaLL;
				}
				else if(GLOBAL_BALLHEIGHT_DELTA > BallPWMDeltaUL)
				{
					GLOBAL_BALLHEIGHT_DELTA = BallPWMDeltaUL;
				}
				
				GLOBALBallPWM += GLOBAL_BALLHEIGHT_DELTA;
				
				if(GLOBALBallPWM < BallPWMLowerLimit)
				{
					GLOBALBallPWM = BallPWMLowerLimit;	
				}
				else if (GLOBALBallPWM > BallPWMUpperLimit)
				{
					GLOBALBallPWM = BallPWMUpperLimit;
				}
				
				TIM4->CCR2 = uint16_t((GLOBALBallPWM + 600)*2);
				
			}
	}
	//ADC1 channel 2 can get the ADC converision result, and store it.
	void ADC1_2_IRQHandler(void)
	{
		ADC_CONVERSION_RESULT = ADC1->DR;
//		USART2SendNumber32(ADC_CONVERSION_RESULT);
//		USART2SendString("\n\r\0");

		ADC1->SR &= 0xFFFFFFE0;
		GLOBAL_ADC_CONVERSION_FINISHED = 1;
	}
}


int main()
{
	int8_t returnedValue = ' ';
	char stringBuff[15] = {' '};
	uint8_t stringBuffLocation = 0;
	char command = '0';
	int32_t resultFromCommand = 0;
	portBInit();//Set all the pins of port B to push-pull, 50MHz
	
	USART2Open();
  NVIC_EnableIRQ(TIM3_IRQn); 
	NVIC_EnableIRQ(TIM2_IRQn);
	NVIC_EnableIRQ(ADC1_2_IRQn);//ADC1_2_IRQHandler
	UIConsole();
	IRSensorInit();


  while(1)
	{
//		while(1)
//		{
//			FindPositionSensor();
//			for (int i = 0; i < 1000000; i++)
//			{
//			}
//		}
		returnedValue = USART2GetByte(' ');
		
		USART2SendByte(returnedValue);
		
		stringBuff[stringBuffLocation] = returnedValue;
		stringBuffLocation++;
		
		//If we recieve a backspace, remove a char, re-write overtop of the old 
		//letter, and then go back a space again.  Don't go back more than 1, if 
		//They hit backspace at the start of the line.
		//We also take care of strings that are too long with this same command.
		if(returnedValue == '\b' || returnedValue == 127 || stringBuffLocation > 13) 
		{
			UIRemoveBackspace(stringBuffLocation);
		}
		if(returnedValue == '\r')
		{
				
			if(stringBuffLocation > 10)
			{
				UIMessageTooLong();
				command = 'H';
			}
			else
			{
				//USART2SendString("In MatchString\n");
				command = UImatchString(stringBuff, stringBuffLocation);
			}
			stringBuffLocation = 0;
			stringBuff[0] = ' ';
			stringBuff[1] = ' ';
			stringBuff[2] = ' ';
			stringBuff[3] = ' ';
			stringBuff[4] = ' ';
			stringBuff[5] = ' ';
			stringBuff[6] = ' ';
			stringBuff[7] = ' ';
			stringBuff[8] = ' ';
			stringBuff[9] = ' ';
			stringBuff[10] = ' ';
			stringBuff[11] = ' ';
			stringBuff[12] = ' ';
			stringBuff[13] = ' ';
			stringBuff[14] = ' ';
			//USART2SendNumber32(stuff);
			resultFromCommand = UILEDCommand(command);
			UIConsole();
		}
		if (resultFromCommand != 0)
		{
			GLOBAL_BALLHEIGHT = resultFromCommand;	
		}
	}
	//USART2Close();
  	
}


