#include "IRSensor.h"

int ADC_CONVERSION_RESULT = 0;

void IRSensorInit(void)
{

	//enable the clocks for port C, and ADC1
	RCC->APB2ENR |= RCC_APB2ENR_IOPCEN | RCC_APB2ENR_ADC1EN; 
	
	//set PCO to input mode with a floating input.
	GPIOC->ODR &= 0x00000000; //set the output bits to zero
	GPIOC->CRL &= 0xFFFFFFF0;
	GPIOC->CRL |= 0x00000004; //GPIOC PIN 0 Set to floating Input
	
	//ADC1->CR2 |= 0x1; //Turn the ADC on
	
	//Setup so just one conversion happens.
	ADC1->SQR1 &= 0xFF000000;
	ADC1->SQR1 |= 0x0;//0x00100000; 
	
	//Channel 10 is our channel that will be processed
	ADC1->SQR3 &= 0xC0000000;
	ADC1->SQR3 |= 0x0000000A; 
	
	
	//Set the sample time for channel 10 to 55.5 cycles
	ADC1->SMPR1 &= 0xFFFFFFF8;
	ADC1->SMPR1 |= 0x5;
	
	ADC1->CR1 |= ADC_CR1_SCAN | ADC_CR1_EOCIE;
	ADC1->CR2 |= ADC_CR2_EXTTRIG | ADC_CR2_EXTSEL | ADC_CR2_ADON;
	
	//reset calibration
	ADC1->CR2 |= ADC_CR2_RSTCAL;
	while ((ADC1->CR2 & ADC_CR2_RSTCAL) == ADC_CR2_RSTCAL)
	{
	}
	
	//Do calibration
	ADC1->CR2 |= ADC_CR2_CAL;
	while ((ADC1->CR2 & ADC_CR2_CAL) == ADC_CR2_CAL)
	{
	}
	
	/*Move on to the control registers. Enable scan mode and interrupt enable for EOC. Enable
external triggering and use SWSTART as the external event select. Also turn the ADC on.
(ADC CR1 and ADC CR2)
	*/
	
		
}
void FindPositionSensor(void) //Can't be run in quick succession!
{
	ADC1->CR2 |= ADC_CR2_SWSTART;
}

int FindPosition(void)
{
	while(ADC1->SR &= 0x1F)
	{
		
	}
	int height = ADC_CONVERSION_RESULT;
	int result = 0;
	
	if (height < 489)
	{
		result = 61;
	}
	else if (height < 626)
	{
		result = LinearInterpolation (489, 61, 626, 56, height);
	}
	else if (height < 716)
	{
		result = LinearInterpolation (626, 56, 716, 51, height);
	}
	else if (height < 789)
	{
		result = LinearInterpolation (716, 51, 789, 46, height);
	}
	else if (height < 830)
	{
		result = LinearInterpolation (789, 46, 830, 41, height);
	}
	else if (height < 907)
	{
		result = LinearInterpolation (830, 41, 907, 36, height);
	}
	else if (height < 1091)
	{
		result = LinearInterpolation (907, 36, 1091, 31, height);
	}
	else if (height < 1317)
	{
		result = LinearInterpolation (1091, 31, 1317, 26, height);
	}
	else if (height < 1900)
	{
		result = LinearInterpolation (1317, 26, 1900, 21, height);
	}
	else if (height < 2431)
	{
		result = LinearInterpolation (1900, 21, 2431, 16, height);
	}
	else if (height < 2526)
	{
		result = LinearInterpolation (2431, 16, 2526, 11, height);
	}
	else if (height < 2563)
	{
		result = 10;
	}
	else if (height < 2626)
	{
		result = 9;
	}
	else if (height < 2826)
	{
		result = 8;
	}
	else if (height < 3131)
	{
		result = 7;
	}
	else if (height < 3515)
	{
		result = 6;
	}
	else if (height < 3790)
	{
		result = 5;
	}
	else
	{
		result = 4;
	}
	return result;
}

int LinearInterpolation (int x1, int y1, int x3, int y3, int x2)
{
	int y2 = y3 + (x2 - x1)*(y3 - y1) / (x3 - x1);
	return y2;
}

void InitializeHeight(void)
{
	//set PWM to 100
}
