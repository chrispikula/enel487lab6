/**
Programmer: Chris Pikula
Project: Ball Control
Date: 2016-11-15

Description: This is part of a program that turns on and off
LEDs via communicating to and from the console.
It can also run some timing tests, as well as use an interrupt timer
	(Via timers 2 and 3, respectively)

It also initializes timer 4, and can run pwm on channel 2.
It can also initialize timer 3, to run the ball controller.
This is the header file for the timers
*/


#include "stm32f10x.h"

void TimerInit(void);
void timer_init_with_interrupts(void);
int16_t timer_start(void);
int16_t timer_stop(int16_t start_time);
void timer_shutdown(void);
void timer3_shutdown(void);
void timer4Init(void);
void Timer2INIT(void);
void timer4shutdown(void);
